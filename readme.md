Installation

run mvn clean package and run install.sh. Shell script copy executable jar
from target folder to ~/.syngen folder, also make a syngen.sh script 
in ~/bin folder (if it doesn't exist script make it). This script run 
syngen jar without annoying java -jar syntax. Be sure then bin folder is 
on PATH for running this script.

Usage

Filler

Filler is use for quick filling of object. 

syngen.sh filler /absolute/path/to/class

This generate setters for object defining in Class in this path. Filler 
use default values in setters arguments for some basic types. If you don't
want fill setters with default values, please use defaults option of filler command:
--defaults=false

Map

Map is use for map one data structure to another. Map command generate 
setters/getters pairs for every field with same name and type in both data 
structures or builder/getters pairs if you choose builder option.

syngen.sh map --from /path/to/source --to /path/to/target

This generate setters/getters, for builder/getters use option 
--type=builder