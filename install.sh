
bin_folder=$1
sg_folder="$HOME/.syngen"


if [[ -z "$bin_folder" ]]; then
    bin_folder="$HOME/bin"
fi

if [[ -d "$sg_folder" ]]; then
    rm -rf "$sg_folder"
fi
mkdir "$sg_folder"
cp target/syngen-1.0-SNAPSHOT-jar-with-dependencies.jar "$sg_folder"/syngen.jar

echo "java -jar $sg_folder/syngen.jar" '$@' > "$bin_folder"/syngen.sh
chmod +x "$bin_folder"/syngen.sh