package cz.svetonaut.syngen.util

fun nameWithFirstLetterUppercase(name: String): String {
    return name.first().uppercase() + name.substring(1)
}