package cz.svetonaut.syngen.processor

import com.github.javaparser.resolution.declarations.ResolvedFieldDeclaration
import com.github.javaparser.symbolsolver.javaparsermodel.declarations.JavaParserFieldDeclaration
import cz.svetonaut.syngen.command.BUILDER
import cz.svetonaut.syngen.command.SETTERS
import cz.svetonaut.syngen.parser.java.JavaAst
import cz.svetonaut.syngen.template.BuilderMappingTemplate
import cz.svetonaut.syngen.template.MappingTemplate
import cz.svetonaut.syngen.template.SetterMappingTemplate
import java.nio.file.Path

class JavaMappingProcessor {

    fun process(fromPath: Path, toPath: Path, type: String): StringBuilder {
        val javaAst = JavaAst()

        val fromType = javaAst.parse(fromPath)
            ?: throw IllegalArgumentException("Problem with resolving of type from this path: $fromPath")
        val toType = javaAst.parse(toPath)
            ?: throw IllegalArgumentException("Problem with resolving of type from this path: $toPath")

        val sb = StringBuilder()

        val allFields: MutableList<ResolvedFieldDeclaration> = fromType.allFields
        val toFieldsMap: Map<String, ResolvedFieldDeclaration> = toType.allFields.associateBy { it.name }

        val mappingTemplate: MappingTemplate = when (type) {
            SETTERS -> SetterMappingTemplate()
            BUILDER -> BuilderMappingTemplate()
            else -> throw IllegalArgumentException("Unknown type of mapping")
        }

        mappingTemplate.doStartOfExpression(sb, toType)

        for (fromField in allFields) {
            val toField = toFieldsMap[fromField.name] ?: continue

            if ((fromField as JavaParserFieldDeclaration).variableDeclarator.typeAsString
                != (toField as JavaParserFieldDeclaration).variableDeclarator.typeAsString) {
                continue
            }

            mappingTemplate.doFieldMapping(sb, fromField, toField)

        }

        mappingTemplate.doEndOfExpression(sb)

        return sb
    }
}