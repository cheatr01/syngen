package cz.svetonaut.syngen.processor

import com.github.javaparser.symbolsolver.javaparsermodel.declarations.JavaParserFieldDeclaration
import cz.svetonaut.syngen.parser.java.JavaAst
import cz.svetonaut.syngen.util.nameWithFirstLetterUppercase
import java.nio.file.Path

class JavaFillerProcessor : FillerProcessor {

    private val valueProcessor = DefaultValueProcessor()

    override fun process(path: Path, defaults: Boolean): StringBuilder {
        val type =
            JavaAst().parse(path)
                ?: throw IllegalArgumentException("Problem with resolving of type from this path: $path")
        val fields = type.allFields

        val sb = StringBuilder()

        for (field in fields) {
            sb.append("entity.set${nameWithFirstLetterUppercase(field.name)}(")

            if (defaults) {
                val typeAsString = (field as JavaParserFieldDeclaration).variableDeclarator.typeAsString
                sb.append(valueProcessor.getDefaultValue(typeAsString, field.name))
            }
            sb.append(");\n")
        }
        return sb
    }
}