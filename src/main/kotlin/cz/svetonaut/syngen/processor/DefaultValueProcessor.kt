package cz.svetonaut.syngen.processor

import cz.svetonaut.syngen.util.nameWithFirstLetterUppercase

class DefaultValueProcessor {

    fun getDefaultValue(typeAsString: String, fieldName: String): String {
        return when (typeAsString) {
            "String" -> "\"test${nameWithFirstLetterUppercase(fieldName)}\""
            "int" -> "1"
            "Integer" -> "1"
            "long" -> "1L"
            "Long" -> "1L"
            "boolean" -> "true"
            "Boolean" -> "true"
            "BigDecimal" -> "BigDecimal.ZERO"
            "LocalDateTime" -> "LocalDateTime.now()"
            "LocalDate" -> "LocalDate.now()"
            "UUID" -> "UUID.randomUUID()"
            else -> "null"
        }
    }
}

