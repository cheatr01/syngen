package cz.svetonaut.syngen.processor

import java.nio.file.Path


interface FillerProcessor {

    fun process(path: Path, defaults: Boolean): StringBuilder
}