package cz.svetonaut.syngen.parser.java

import com.github.javaparser.StaticJavaParser
import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration
import com.github.javaparser.symbolsolver.JavaSymbolSolver
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver
import java.nio.file.Path

class JavaAst {

    fun parse(path: Path): ResolvedReferenceTypeDeclaration? {
        val combinedTypeSolver = CombinedTypeSolver();
        combinedTypeSolver.add(ReflectionTypeSolver());

        val symbolSolver = JavaSymbolSolver(combinedTypeSolver);
        StaticJavaParser.getConfiguration().setSymbolResolver(symbolSolver);

        val cu = StaticJavaParser.parse(Path.of(path.toUri()))
        return cu.types.first.get().resolve()
    }
}