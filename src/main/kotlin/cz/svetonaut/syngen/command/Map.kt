package cz.svetonaut.syngen.command

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import cz.svetonaut.syngen.processor.JavaMappingProcessor
import java.nio.file.Path
import kotlin.system.exitProcess

class Map : CliktCommand(help = "Generate code for transfer data from one class to another.") {
    private val fromPathString by option(
        names = arrayOf("--from"),
        help = "Absolute path to class from we map"
    ).required()
    private val toPathString by option(
        names = arrayOf("--to"),
        help = "Absolute path to class to we map"
    ).required()
    private val typeOfMapping by option(
        names = arrayOf("--type"),
        help = "Type of mapping. We support $SETTERS/$BUILDER. Setters is default option."
    )
        .default(SETTERS)

    override fun run() {
        if (fromPathString.isBlank() || toPathString.isBlank()) {
            echo("Invalid path inserted")
            exitProcess(1)
        }

        val fromPath = Path.of(fromPathString)
        val toPath = Path.of(toPathString)
        if (!fromPath.isAbsolute || !toPath.isAbsolute) {
            println("You must insert an absolute path of file")
            return
        }

        val sb = JavaMappingProcessor().process(fromPath, toPath, typeOfMapping)

        echo(sb.toString())
    }
}

const val SETTERS = "setters"
const val BUILDER = "builder"