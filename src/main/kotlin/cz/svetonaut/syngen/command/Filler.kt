package cz.svetonaut.syngen.command

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import cz.svetonaut.syngen.processor.JavaFillerProcessor
import kotlin.io.path.Path

class Filler: CliktCommand(help = "Generate setters for all fields with default values when possible") {
    private val path by argument(help = "Absolute path to entity class")
    private val defaults by option("-d", "--defaults", help = "Fill it with defaults values?").default("y")

    override fun run() {
        if (path.isBlank()){
            echo("Path must not be empty")
            return
        }

        val entityPath = Path(path)

        if (!entityPath.isAbsolute) {
            echo("Path must be absolute")
            return
        }

        val processor = JavaFillerProcessor()
        val sb = processor.process(entityPath, defaults == "y")

        echo(sb.toString())
    }

}