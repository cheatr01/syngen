package cz.svetonaut.syngen.template

import com.github.javaparser.resolution.declarations.ResolvedFieldDeclaration
import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration
import cz.svetonaut.syngen.util.nameWithFirstLetterUppercase

class SetterMappingTemplate : MappingTemplate {
    override fun doStartOfExpression(sb: StringBuilder, toType: ResolvedReferenceTypeDeclaration) {
        //nothing for start
    }

    override fun doFieldMapping(
        sb: StringBuilder,
        fromField: ResolvedFieldDeclaration,
        toField: ResolvedFieldDeclaration,
    ) {
        sb.appendLine(
            "to.set${nameWithFirstLetterUppercase(toField.name)}(from.get${
                nameWithFirstLetterUppercase(
                    fromField.name
                )
            });"
        )
    }

    override fun doEndOfExpression(sb: StringBuilder) {
        //nothing for end
    }

}