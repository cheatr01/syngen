package cz.svetonaut.syngen.template

import com.github.javaparser.resolution.declarations.ResolvedFieldDeclaration
import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration
import cz.svetonaut.syngen.util.nameWithFirstLetterUppercase

class BuilderMappingTemplate : MappingTemplate {
    override fun doStartOfExpression(sb: StringBuilder, toType: ResolvedReferenceTypeDeclaration) {
        sb.appendLine("${toType.name}.builder()")
    }

    override fun doFieldMapping(
        sb: StringBuilder,
        fromField: ResolvedFieldDeclaration,
        toField: ResolvedFieldDeclaration,
    ) {
        sb.appendLine(".${toField.name}(from.get${nameWithFirstLetterUppercase(fromField.name)}())")
    }

    override fun doEndOfExpression(sb: StringBuilder) {
        sb.appendLine(".build();")
    }
}