package cz.svetonaut.syngen.template

import com.github.javaparser.resolution.declarations.ResolvedFieldDeclaration
import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration

interface MappingTemplate {

    fun doStartOfExpression(sb: StringBuilder, toType: ResolvedReferenceTypeDeclaration)

    fun doFieldMapping(sb: StringBuilder, fromField: ResolvedFieldDeclaration, toField: ResolvedFieldDeclaration)

    fun doEndOfExpression(sb: StringBuilder)
}