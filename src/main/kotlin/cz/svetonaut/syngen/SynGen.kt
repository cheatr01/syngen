package cz.svetonaut.syngen

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import cz.svetonaut.syngen.command.Filler
import cz.svetonaut.syngen.command.Map

fun main(args: Array<String>) {

    try {
        SynGen()
            .subcommands(
                Map(),
                Filler(),
            )
            .main(args)

    } catch (e: Exception) {
        println("Run of program ends with error: ${e.message}")
    }
}

class SynGen : CliktCommand(name = "syngen", help = "Syntax Generator") {
    override fun run() = Unit
}