package cz.svetonaut.syngen.parser.java

import java.nio.file.Path
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import org.junit.jupiter.api.Test

internal class JavaAstTest {

    @Test
    fun parse() {
        val javaAst = JavaAst()
        val resolved =
            javaAst.parse(Path.of("src/test/java/cz/svetonaut/syngen/TestDto.java"))
        assertNotNull(resolved)
        assertEquals("TestDto", resolved.name)
    }
}