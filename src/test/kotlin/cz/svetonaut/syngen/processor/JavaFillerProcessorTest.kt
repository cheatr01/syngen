package cz.svetonaut.syngen.processor

import org.junit.jupiter.api.Test
import java.nio.file.Path
import kotlin.test.assertEquals

internal class JavaFillerProcessorTest {

    @Test
    fun `test process - correct call with defaults - correct return`() {
        val underTest = JavaFillerProcessor()

        val sb = underTest.process(
            Path.of("src/test/java/cz/svetonaut/syngen/TestDto.java"),
            true
        )

        val string = """
        entity.setBool(true);
        entity.setNumber(1);
        entity.setTestString("testTestString");
        
        """.trimIndent()

        println(sb.toString())

        assertEquals(string, sb.toString())
    }

    @Test
    fun `test process - correct call with no defaults - correct return`() {
        val underTest = JavaFillerProcessor()

        val sb = underTest.process(
            Path.of("src/test/java/cz/svetonaut/syngen/TestDto.java"),
            false
        )

        val string = """
        entity.setBool();
        entity.setNumber();
        entity.setTestString();
        
        """.trimIndent()

        println(sb.toString())

        assertEquals(string, sb.toString())
    }
}