package cz.svetonaut.syngen.processor

import cz.svetonaut.syngen.command.BUILDER
import cz.svetonaut.syngen.command.SETTERS
import java.nio.file.Path
import kotlin.test.assertEquals
import org.junit.jupiter.api.Test

internal class JavaMappingProcessorTest {

    @Test
    fun test_process_setters() {
        val underTest = JavaMappingProcessor()
        val sb = underTest.process(
//            File(URL("./../../../../../java/cz/svetonaut/syngen/TestEntity.java").toURI()).toPath(),
            Path.of("src/test/java/cz/svetonaut/syngen/TestEntity.java"),
            Path.of("src/test/java/cz/svetonaut/syngen/TestDto.java"),
            SETTERS
        )

        val expected = """
            to.setBool(from.getBool);
            to.setNumber(from.getNumber);
            to.setTestString(from.getTestString);
            
        """.trimIndent()

        assertEquals(expected, sb.toString())
    }

    @Test
    fun test_process_builder() {
        val underTest = JavaMappingProcessor()
        val sb = underTest.process(
            Path.of("src/test/java/cz/svetonaut/syngen/TestEntity.java"),
            Path.of("src/test/java/cz/svetonaut/syngen/TestDto.java"),
            BUILDER
        )

        val expected = """
            TestDto.builder()
            .bool(from.getBool())
            .number(from.getNumber())
            .testString(from.getTestString())
            .build();
            
        """.trimIndent()

        assertEquals(expected, sb.toString())
    }
}